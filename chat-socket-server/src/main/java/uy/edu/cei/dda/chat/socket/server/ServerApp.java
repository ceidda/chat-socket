package uy.edu.cei.dda.chat.socket.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import uy.edu.cei.dda.chat.socket.common.Message;

/**
 * Hello world!
 *
 */
public class ServerApp {
	private static final int SERVER_PORT = 1999;
	private static final Logger logger = LoggerFactory.getLogger(ServerApp.class);

	public static void main(String[] args) throws IOException {
		new ServerApp().runServer();
	}

	private List<SocketClient> clients = new LinkedList<>();
	private ObjectMapper objectMapper;
	
	public ServerApp() {
		this.objectMapper = new ObjectMapper();
	}

	public void runServer() throws IOException {
		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
			while (true) {
				logger.info("waiting for clients");
				Socket socket = serverSocket.accept();
				SocketClient socketClient = new SocketClient(this, socket);
				this.clients.add(socketClient);
				Message msg = new Message("un cliente se unio");
				String json = this.objectMapper.writeValueAsString(msg);
				this.clients.forEach((c) -> c.update(json));
			}
		}
	}

	public void publishMessage(String message) {			
		this.clients.forEach((c) -> c.update(message));
	}
	
	private void removeThread(SocketClient socketClient) {
		this.clients.remove(socketClient);
	}

	class SocketClient extends Thread implements Observer {
		private final ServerApp serverApp;
		private final Socket socket;
		private BufferedReader in;
		private final PrintWriter out;

		public SocketClient(final ServerApp serverApp, final Socket socket) throws IOException {
			this.serverApp = serverApp;
			this.socket = socket;
			this.out = new PrintWriter(this.socket.getOutputStream(), true);
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			super.start();
		}
		
		@Override
		public void run() {
			try {
				String message = null;
				while((message = this.in.readLine()) != null) {
					this.serverApp.publishMessage(message);
				}
			} catch (IOException e) {
				logger.error("error", e);
			} finally {
				this.out.close();
				try {
					this.in.close();
				} catch (IOException e1) {
					logger.warn("nothing to do, connection lost?", e1);
				}
				try {
					this.socket.close();
				} catch (IOException e1) {
					logger.warn("nothing to do, connection lost?", e1);
				}
				logger.info("client disconnected");
				this.serverApp.removeThread(this);
			}
		}
		
		@Override
		public void update(String message) {
			this.out.println(message);			
		}
	}
	
	interface Observer {
		public void update(String message);
	}
}
