package uy.edu.cei.dda.chat.socket.client.common;

import uy.edu.cei.dda.chat.socket.common.Message;

public interface Observer {
	public void update(Message message);
}
