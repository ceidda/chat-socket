package uy.edu.cei.dda.chat.socket.client.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import uy.edu.cei.dda.chat.socket.client.common.Observer;
import uy.edu.cei.dda.chat.socket.client.service.ServerClientService;
import uy.edu.cei.dda.chat.socket.common.Message;

import javax.swing.SpringLayout;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainWindow implements Observer {

	private JFrame frame;
	private JTextArea textArea;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
					ServerClientService.getInstance().add(window);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				ServerClientService.getInstance().disconnect();
			}
		});
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		textArea = new JTextArea();
		springLayout.putConstraint(SpringLayout.NORTH, textArea, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textArea, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, textArea, 239, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textArea, 440, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textArea);
		
		textField = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField, 3, SpringLayout.SOUTH, textArea);
		springLayout.putConstraint(SpringLayout.WEST, textField, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ServerClientService.getInstance().sendMessage(textField.getText());
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 3, SpringLayout.SOUTH, textArea);
		springLayout.putConstraint(SpringLayout.EAST, textField, -6, SpringLayout.WEST, btnNewButton);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, 0, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(btnNewButton);
	}

	@Override
	public void update(Message message) {
		this.textArea.append(message.getText());
		this.textArea.append("\n");
	}
}
