package uy.edu.cei.dda.chat.socket.client.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import uy.edu.cei.dda.chat.socket.client.common.Observer;
import uy.edu.cei.dda.chat.socket.common.Message;

public class ServerClientService extends Thread {

	private static final int SERVER_PORT = 1999;
	private static ServerClientService instance = null;
	private static final Logger logger = LoggerFactory.getLogger(ServerClientService.class);

	static {
		try {
			instance = new ServerClientService();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static ServerClientService getInstance() {
		return instance;
	}

	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	private List<Observer> observers;
	private ObjectMapper objectMapper;

	private ServerClientService() throws UnknownHostException, IOException {
		this.socket = new Socket("localhost", SERVER_PORT);
		this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.out = new PrintWriter(this.socket.getOutputStream(), true);
		this.observers = new LinkedList<Observer>();
		this.objectMapper  = new ObjectMapper();
		super.start();
	}

	@Override
	public void run() {
		String message = null;
		try {
			while ((message = in.readLine()) != null) {
				for (Observer o : this.observers) {
					logger.info("antes de deserealizar: {}", message);
					Message msg = this.objectMapper.readValue(message, Message.class);
					o.update(msg);
				}
			}
		} catch (IOException e) {
			logger.error("error", e);
		}
	}
	
	public void sendMessage(String message) {
		try {
			Message msg = new Message(message);
			String json = this.objectMapper.writeValueAsString(msg);
			logger.info(json);
			this.out.println(json);
		} catch (IOException e) {
			logger.error("buuu", e);
		}
	}

	public void add(Observer o) {
		this.observers.add(o);
	}

	public void disconnect() {
		this.out.close();
		try {
			this.in.close();
		} catch (IOException e) {
			logger.error("fail to close connection", e);
		}
		try {
			this.socket.close();
		} catch (IOException e) {
			logger.error("fail to close connection", e);
		}
	}
}
