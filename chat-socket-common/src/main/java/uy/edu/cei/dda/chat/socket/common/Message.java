package uy.edu.cei.dda.chat.socket.common;

import java.io.Serializable;

public class Message implements Serializable {

	private String author;
	private String text;

	public Message() {
	}

	public Message(String text) {
		this.text = text;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
